FROM docker.io/debian:stable

RUN apt-get update \
 && apt-get install -y git wget flex bison gperf python3 python3-pip python3-setuptools cmake ninja-build ccache libffi-dev libssl-dev dfu-util libusb-1.0-0 unzip python3-virtualenv

COPY --chmod=777 entrypoint.sh /entrypoint.sh

RUN useradd -G dialout -m -s /bin/bash user
USER user:user

ADD --chown=user:user https://github.com/espressif/esp-idf/releases/download/v4.4.7/esp-idf-v4.4.7.zip /home/user

WORKDIR /home/user

# ./install.sh esp32,esp32c3,esp32s3

RUN unzip esp-idf-v4.4.7.zip \
 && mv esp-idf-v4.4.7 esp-idf \
 && cd esp-idf/ \
 && ./install.sh esp32

VOLUME /home/user/code
WORKDIR /home/user/code

ENTRYPOINT ["/entrypoint.sh"]
CMD ["/bin/bash"]
