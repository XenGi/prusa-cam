# Prusa Cam

Small [MicroPython][micropython] app to use a [ESP32-POE][esp32-poe] and a [OV5640][ov5640] Camera Board as a camera for [Prusa Connect][prusa-connect].

## Hardware setup

__tba__

## Setup Prusa Cam

1. Copy `settings.toml.example` to `settings.toml`
2. Add a new Camera to Prusa Connect
   1. Open the WebUI: https://connect.prusa3d.com/
   2. Go to the Camera tab
   3. Use the `➕ Add new other Camera` button to add a new camera
   4. Get the Token from the UI and save it to the `settings.toml` file
3. Go to the Settings tab and get Printer UUID from the UI
4. Save the Printer UUID to the `settings.toml` file
5. Create unique Fingerprint for your camera
   1. Use this command to generate random UUID:
      ```shell
      uuidgen -r
      ```
   2. Save the Fingerprint UUID to the `settings.toml` file
6. Add your Wifi credentials to the `settings.toml` file
7. Setup ESP32-POE board
   1. Replace `/dev/ttyUSB0` in `Makefile` with your serial port
   2. Flash firmware, install dependencies and upload files to the ESP-EYE board:
      ```shell
      make deploy
      ```
8. Reset ESP32-POE board
   ```shell
   ampy --port /dev/ttyUSB0 reset
   ```
9. If camera pictures don't show up in Prusa Connect, check for errors via serial:
   ```shell
   minicom -D/dev/ttyUSB0
   ```

## Tests with curl

Configure camera (also registers camera):

```shell
curl -v -X PUT \
    -H "Token: $PRUSA_TOKEN" \
    -H "Fingerprint: $PRUSA_FINGERPRINT" \
    --json @config.json \
    https://connect.prusa3d.com/c/info
```

Upload image:

```shell
curl -v -X PUT \
    -H "Token: $PRUSA_TOKEN" \
    -H "Fingerprint: $PRUSA_FINGERPRINT" \
    -H "Content-Type: image/jpg" \
    --data-binary @benchy.jpg \
    https://webcam.connect.prusa3d.com/c/snapshot
```

## Issues

### Timeouts when sending images

Even with the timeout value for the socket set to 10s, The image upload still receives a timeout. The JPEG data seems to
reach Prusa connect but the HTTP request is not successful.

### Image capture fails from time to time

Sometimes the Camera doesn't work and the `take()` function returns no data.

## Notes

- MicroPython for the ESP32-POE: https://micropython.org/download/OLIMEX_ESP32_POE/
- Some inspiration: https://gist.github.com/nunofgs/84861ee453254823be6b069ebbce9ad2
- Arduino lib: https://github.com/0015/ESP32-OV5640-AF
- Waveshare OV5640 breakout: https://www.waveshare.com/ov5640-camera-board-c.htm
- Adafruit OV5640 breakout: https://www.adafruit.com/product/5673

---

Made with 💖 and 🐍.


[micropython]: https://micropython.org/
[esp32-poe]: https://www.olimex.com/Products/IoT/ESP32/ESP32-POE/open-source-hardware
[ov5640]: https://www.waveshare.com/ov5640-camera-board-b.htm
[prusa-connect]: https://connect.prusa3d.com/

